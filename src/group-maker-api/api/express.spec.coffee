chai = require 'chai'
expect = chai.expect

{handler} = require './express'

describe 'group-maker-api:handler', ->
    it 'takes a request event and returns the matched pairs as items', ->
        expectedUsers = ['a', 'b']
        testEvent = {body: {users: expectedUsers}}
        expectedOutput = {items: [expectedUsers]}
        result = handler.handle testEvent
        expect(result).to.eql expectedOutput