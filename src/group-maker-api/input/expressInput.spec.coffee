chai = require 'chai'
expect = chai.expect

{expressRequestInputParser} = require './expressInput'

describe 'expressRequestInputParser', ->
    it 'takes a request event and returns the users collection from body', ->
        expectedUsers = ['a', 'b', 'c']
        testEvent = {body: {users: expectedUsers}}
        result = expressRequestInputParser.getData(testEvent)
        expect(result).to.equal(expectedUsers)
